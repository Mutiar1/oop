<?php
require_once('animal.php');
require_once('kodok.php');
require_once('sungokong.php');
$animal = new Animal('Shaun'); 
echo "Name : ".$animal->name ."<br>";
echo "legs : ".$animal->legs ."<br>";
echo "cold blooded : ".$animal->cold_blooded ."<br><br>";

$kodok = new Frog ('buduk');
echo "Name : ".$kodok->name ."<br>";
echo "legs : ".$kodok->legs ."<br>";
echo "cold blooded : ".$kodok->cold_blooded ."<br>";
echo "Jump : ".$kodok->jump ."<br><br>";

$sungokong = new Ape ('kera sakti');
echo "Name : ".$sungokong->name ."<br>";
echo "legs : ".$sungokong->legs ."<br>";
echo "cold blooded : ".$sungokong->cold_blooded ."<br>";
echo "Yell : ".$sungokong->yell ."<br>";
?>